#include <iostream>
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "opencv2/calib3d/calib3d.hpp"
#include <stdio.h>
#include <stdlib.h>



using namespace cv;
using namespace std;

int main(int argc,char** argv)
{
 Mat img1;
 Mat img2;


 img1 = imread("img1.jpg",1);
 img2 = imread("img2.jpg",1);


 if(!img1.data )
 {
     cout<<"image 1 has not loaded"<<endl;
     return 1;
 }
 if(!img2.data )
 {
     cout<<"image 2 has not loaded"<<endl;
     return 1;
 }


 namedWindow("img1",CV_WINDOW_NORMAL);



     vector<KeyPoint> keypoints1;
     vector<KeyPoint> keypoints2;


     SurfFeatureDetector detector(1000);

    detector.detect(img1,keypoints1);
    detector.detect(img2,keypoints2);



    SurfDescriptorExtractor extractor;

    Mat extracted_img1,extracted_img2;

   extractor.compute(img1,keypoints1,extracted_img1);
   extractor.compute(img2,keypoints2,extracted_img2);


    FlannBasedMatcher matcher;
    std::vector< DMatch > matches;
    matcher.match( extracted_img1, extracted_img2, matches );

    double max_dist = 0; double min_dist = 100;

    //-- Quick calculation of max and min distances between keypoints
    for( int i = 0; i < extracted_img1.rows; i++ )
    { double dist = matches[i].distance;
      if( dist < min_dist ) min_dist = dist;
      if( dist > max_dist ) max_dist = dist;
    }

    printf("-- Max dist : %f \n", max_dist );
    printf("-- Min dist : %f \n", min_dist );

    std::vector< DMatch > good_matches;

    for( int i = 0; i < extracted_img1.rows; i++ )
    { if( matches[i].distance < 2*min_dist )
      { good_matches.push_back( matches[i]); }
    }



    vector<Point2f> points1 ,points2;
    for( int i = 0; i < good_matches.size(); i++ ) {
        points1.push_back( keypoints1[ good_matches[i].queryIdx ].pt );
        points2.push_back(keypoints2[ good_matches[i].trainIdx ].pt );
    }

    std::cout<<points1.size()<<std::endl;
    std::cout<<points2.size()<<std::endl;

    Mat H = findHomography(points2,points1, CV_RANSAC);

    Mat H1 =  H.inv();

    //H1 = H * H1;

    Mat H_inverted;


    warpPerspective(img2,img2,Mat::eye(Size(3,3),CV_64F),Size(img1.cols*2,img1.rows*2),INTER_CUBIC);

    img2.copyTo(H_inverted);

    Mat img1Copy;

    img1.copyTo(img1Copy);

    cvtColor(img1Copy,img1Copy,CV_RGB2GRAY);

    int height = img1.cols;
    int width = img1.rows;

    warpPerspective(img1,img1,H1,Size(height*2,width*2),INTER_CUBIC);

    warpPerspective(img1Copy,img1Copy,H1,Size(height*2,width*2),INTER_CUBIC);

    img1.copyTo(H_inverted,img1Copy);







    //-- Draw only "good" matches
    Mat img_matches;
    drawMatches( img1, keypoints1, img2, keypoints2,
                 good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                 vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );

    //-- Show detected matches
    imshow( "img1", H_inverted );


   //homography






   waitKey(0);


    return 0;
}

